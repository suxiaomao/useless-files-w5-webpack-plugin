# useless-files-w5-webpack-plugin

## 文档

[中文文档](https://gitee.com/suxiaomao/useless-files-w5-webpack-plugin/blob/master/README.cn.md)，[英文文档](https://gitee.com/suxiaomao/useless-files-w5-webpack-plugin/blob/master/README.md)。

## 简介

useless-files-w5-webpack-plugin 是一个监听 webpack 项目没有被引用或者被使用的文件插件，它兼容 webpack5 和 webpack5 以前的版本。

## 参数

### root

    需要被监听的目录，可以是一个数组或者是一个字符串。

### clean

    编译完成后是否需要删除没有被引用的或者没有被使用的文件，值可以是true/false。

### excludeSuffix

    排除哪些后缀不需要被监听，该后缀文件是基于root的目录文件下的，它是一个数组类型，可排除多个后缀文件。例如后缀文件为'.xx/.xx.xx/.xx.xx.xx'。

### output

    编译完成后把没有被引用的或者没有被使用的文件输出到指定的文件。

### exclude

    排除哪些文件不需要被监听，可以排除多个文件，它是基于root下面的文件，值可以为数组或者字符串。

## 例子

```js
 module.exports = {
    ....
    plugins:[
         new UnusedFilesW5WebpackPlugin({
          root: ['./src', './mock'] // 可以是数组或者字符串,
          clean: false, //可以是true或者false
          excludeSuffix: ['.d.ts','.js'], // 只能是一个数组
          output: './useless-files.json', // 只能是一个字符串
          exclude: ['xxx.mock.js','m.xxx.js'], // 可以是字符串或者数组,
        }),
    ]
 }
```

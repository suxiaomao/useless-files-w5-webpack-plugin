# useless-files-w5-webpack-plugin

## homepage

[CN doc](https://gitee.com/suxiaomao/useless-files-w5-webpack-plugin/blob/master/README.cn.md) , [EN doc](https://gitee.com/suxiaomao/useless-files-w5-webpack-plugin/blob/master/README.md).

## intro

useless-files-w5-webpack-plugin is watch webpack project not quoted files webpack plugin.

The plugin is compatible with webpack 5.

## params

### root

    Directories to observe,multiple directories can be observed,It can be an array or a string.

### clean

    Whether to delete files that are not referenced. It can be an true or false.

### excludeSuffix

    There is no need for the observed suffix file, which is based in the root directory. it can ".xx" or ".xx.xx" or ".xx.xx.xx" ...

### output

    Unreferenced file, output path.

### exclude

    Files that do not need to be observed, change files based on root directory. It can be an array or a string.

## example

```js
 module.exports = {
    ....
    plugins:[
         new UnusedFilesW5WebpackPlugin({
          root: ['./src', './mock'] // It can also be a string './xxxx',
          clean: false,
          excludeSuffix: ['.d.ts','.js'],
          output: './useless-files.json',
          exclude: ['xxx.mock.js','m.xxx.js'], // t can also be a string 'xxxx.js',
        }),
    ]
 }
```
